use secrecy::Secret;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize)]
pub struct TokenFromSharedSecretRequestBody<'a> {
    pub grant_type: &'a str,
    pub client_id: &'a str,
    pub client_secret: &'a str,
    pub resource: &'a str,
}

#[derive(Debug, Deserialize)]
pub struct TokenResponse {
    pub access_token: Secret<String>,
    pub token_type: String,
    pub expires_in: String,
    pub expires_on: String,
    pub not_before: String,
    pub resource: String,
}

#[derive(Debug, Deserialize)]
pub struct ReadSecretResponse {
    pub value: Secret<String>,
    pub id: String,
    // omitted: attributes
}

#[derive(Debug, Clone, Deserialize)]
pub struct TokenRetrievalErrorResponse {
    pub error: String,
    pub error_description: String,
    pub error_codes: Vec<i64>,
    pub timestamp: String,
    pub trace_id: String,
    pub correlation_id: String,
    pub error_uri: String,
}

#[derive(Debug, Clone, Deserialize)]
pub struct KeyVaultErrorResponse {
    pub error: KeyVaultError,
}

#[derive(Debug, Clone, Deserialize)]
pub struct KeyVaultError {
    pub code: String,
    pub message: String,
}
